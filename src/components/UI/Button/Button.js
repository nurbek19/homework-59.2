import React, {Component} from 'react';

class Button extends Component {
    shouldComponentUpdate() {
        return false;
    }

    render() {
        return(
            <button onClick={this.props.click}>New Jokes</button>
        )
    }
}

export default Button;