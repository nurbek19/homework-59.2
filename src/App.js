import React, {Component} from 'react';
import Joke from './components/Joke/Joke';
import Button from './components/UI/Button/Button';
import './App.css';

class App extends Component {
    state = {
        jokes: []
    };

    _requestURL(url) {
        return fetch(url).then(
            response => {
                if (response.ok) {
                    return response.json();
                }

                throw new Error('Something went wrong');
            }
        )
    }

    componentDidMount() {
        const JOKE_URL = 'https://api.chucknorris.io/jokes/random';
        const promisesArrray = [];
        let i = 0;

        while (i < 5) {
            promisesArrray.push(this._requestURL(JOKE_URL));
            i++;
        }

        Promise.all(promisesArrray).then(array => {

                const jokesArray = array.map(joke => {
                    return {
                        id: joke.id,
                        text: joke.value
                    }
                });

                this.setState({jokes: jokesArray});
            }
        );
    }

    getJokes = () => {
       this.componentDidMount();
    };

    render() {
        return (
            <div className="App">
                {this.state.jokes.map((joke, index) => <Joke key={index} text={joke.text}/>)}

                <div style={{width: '100%', textAlign: 'center', marginTop: '20px'}}>
                    <Button click={this.getJokes}/>
                </div>
            </div>
        );
    }
}

export default App;
